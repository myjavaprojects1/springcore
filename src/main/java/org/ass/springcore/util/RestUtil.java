package org.ass.springcore.util;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component 
//@Scope("singleton")
public class RestUtil {
	@Value("https://192.168.1.100/spring")
	private String url;
	
	@Autowired
	private CommonUtil commonUtil;
	
	public RestUtil() {
		System.out.println("this is RestUtil class constructor");
		
	}
	
	public void post() {
		System.out.println(commonUtil.convertStringToDate("29-10-2023"));
	}
	

	@Override
	public String toString() {
		return "RestUtil [url=" + url + "]";
	}
}
