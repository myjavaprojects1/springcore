package org.ass.springcore.util;

import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class CommonUtil {

	
	public CommonUtil() {
		System.out.println(this.getClass().getSimpleName());
	}
	
	public Date convertStringToDate(String date) {
		return new Date(date);
	}
}
