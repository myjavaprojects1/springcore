package org.ass.springcore.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("org.ass.springcore")
public class AppConfig {
	
	@Bean
	public org.hibernate.cfg.Configuration getConfiguration(){
		return new org.hibernate.cfg.Configuration();
	}
}
